Django==1.9.8
argparse==1.2.1
wsgiref==0.1.2
django-bower==5.1.0
six==1.10.0
django-bootstrap-form==3.2.1
slacker==0.9.24

