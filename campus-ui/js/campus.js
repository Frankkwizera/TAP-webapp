/*function to auto-generate a CRSF cookie */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies;
        cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

//changing char field into password field
if ($('#id_password').length){
   document.getElementById('id_password').setAttribute('type','password');
   document.getElementById('id_Re_password').setAttribute('type','password');
}
// reviewing an article
$(".review").click(function(){
  var articleId = $(".review").val();
  $.post("/reviewed",{csrfmiddlewaretoken: csrftoken, "articleId":articleId},function(data){
    alert(data);
    if (data == "reviewed"){
        window.location.assign('/review-page');
    }
  });
});
// deleting un reviewed article
$(".delete").click(function(){
  var articleId = $(".delete").val();
  $.post("/delete-article",{csrfmiddlewaretoken: csrftoken, "articleId":articleId},function(data){
    alert(data);
    if (data == "deleted"){
        window.location.assign('/review-page');
    }
  });

});

// searching
/*
$("#search-text").on('change keydown paste input', function(){
   var text = $('#search-text').val();
   $.get("/search",{'search-article':text});
});
*/
$('.encode').each(function(){
    var $this = $(this);
    var t = $this.text();
    $this.html(t.replace('&lt','<').replace('&gt', '>'));
});

//sending feedback message
var feedbackModal = $('#alertModal');
$('#ohereza').click(function(){
   var message = $('#feedbackMessage').val(),
       email = $('#feedbackEmail').val(),
       names = $('#feedbackNames').val();
       if(!message || !email || !names){
          feedbackModal.modal('show');
          $(".alert-message").text("Please Fill in your Names, Email, and the Message. Thanks")
       }else{
         $.post("/feedback",{csrfmiddlewaretoken: csrftoken, "message":message,"email":email,"names":names},function(data){
           feedbackModal.modal('show');
           $(".alert-message").text("Hi "+names+", "+ data)
         });
       }
});

// sending comments to djangobackend
$('#sendComment').click(function(){
      var commentName = $('#commentName').val(),
          commentEmail = $('#commentEmail').val(),
          commentMessage = $('#commentMessage').val()
          commentedArticle = $('#articleId').val();
          // checking if user filled required fileds
          if (!commentName || !commentEmail || ! commentMessage){
            feedbackModal.modal('show');
            $(".alert-message").text("Please Fill in your Names, Email, and the Message. Thanks")
          }else {
            $.post("/comments",{csrfmiddlewaretoken:csrftoken,"commentedArticle":commentedArticle,
                                                              "commentName":commentName,
                                                              "commentEmail":commentEmail,
                                                              "commentMessage":commentMessage},function(data){
                                                                feedbackModal.modal('show');
                                                                  $(".alert-message").text(data)
                                                                  $('#commentform')[0].reset();
                                                              });
          }
});
