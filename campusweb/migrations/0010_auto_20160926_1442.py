# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-09-26 12:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campusweb', '0009_auto_20160907_0145'),
    ]

    operations = [
        migrations.AddField(
            model_name='writer',
            name='profile',
            field=models.FileField(blank=True, help_text='profile picture', null=True, upload_to='media'),
        ),
        migrations.AlterField(
            model_name='newsarticle',
            name='time',
            field=models.DateTimeField(default=0),
        ),
    ]
