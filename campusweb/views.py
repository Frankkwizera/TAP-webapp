from django.shortcuts import render
from campusweb.models import *
from campusweb.forms import *
from django.http import HttpResponse
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from utils import *
from django.db import IntegrityError
from django.contrib.auth.models import User
from django.forms import formset_factory
from datetime import datetime
from django.forms import inlineformset_factory
from django.shortcuts import redirect
#system default encoding as utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# from PIL import Image
# Create your views here.

def index(request):
    """ data to be displayed in the main page """
    context_dict = {}

    date = datetime.today()
    context_dict['weekday'] = date.strftime("%A")
    context_dict['today']  = date.strftime("%d")
    context_dict['month'] = date.strftime("%B")
    context_dict['year'] = date.strftime("%Y")
    NewsArticles = main_page_data(request)
    topArticle = NewsArticles[0]
    topSideArticle = NewsArticles[1]
    topLastArticle = NewsArticles[2]
    secondTopArticle = NewsArticles[3]

    id_to_exclude = [topArticle.id,topSideArticle.id,topLastArticle.id,secondTopArticle.id]
    all_Articles = NewsArticle.objects.filter(reviewed=True).exclude(id__in = id_to_exclude).order_by('-time')
    popular_Articles = NewsArticle.objects.filter(reviewed = True).order_by('-time')[:4]

    context_dict['popularArticles'] = popular_Articles
    context_dict['topArticle'] = topArticle
    context_dict['topSideArticle'] = topSideArticle
    context_dict['topLastArticle'] = topLastArticle
    context_dict['secondTopArticle'] = secondTopArticle
    #context_dict['articles'] = NewsArticles
    context_dict['articles'] = all_Articles
    context_dict['categories'] = Category.objects.all()
    return render(request,'main-page.html',context_dict)

def login(request):
    """ Rendering login page """
    context_dict = {}
    context_dict['categories'] = Category.objects.all()
    if request.session.has_key('logged'):
        user = request.user
        writer = Writer.objects.filter(user=user).first()
        new_article = ArticleForm()
        photoformset = formset_factory(PhotoForm)
        formset = photoformset()
        context_dict['formset'] = formset
        context_dict['writer'] = writer
        context_dict['newarticle'] = new_article
        context_dict['categories'] = Category.objects.all()
        return render(request, 'new-article.html',context_dict)
    return render(request, 'login.html',context_dict)

def log_in(request):
    """ Validating a user """
    context_dict = {}
    if request.method == 'POST':
        username = request.POST.get('username','')
        password = request.POST.get('password','')

        user = auth.authenticate(username = username,password=password)

        if user is not None:
            if user.is_active:
                auth.login(request,user)
                request.session['logged'] = True
                request.session.save()
                writer = Writer.objects.filter(user=user).first()
                message = writer.first_name + writer.second_name + " logged in"
                try:
                    slack_notification("general",message)
                except Exception,e:
                    pass
                new_article = ArticleForm()
                photoformset = formset_factory(PhotoForm)
                formset = photoformset()
                context_dict['formset'] = formset
                context_dict['writer'] = writer
                context_dict['newarticle'] = new_article
                context_dict['categories'] = Category.objects.all()
            else:
                context_dict['message'] = "Please contact system Admin"
        else:
            context_dict['message'] = "invalid username or password"
            return render(request, 'login.html',context_dict)
    return render(request, 'new-article.html',context_dict)

def log_out(request):
    """function to logout a user"""
    auth.logout(request)
    context_dict = {}
    if 'logged' in request.session:
        del request.session['logged']

    context_dict['categories'] = Category.objects.all()
    context_dict['articles'] = main_page_data(request)
    return render(request,'main-page.html',context_dict)

@login_required
def new_article_page(request):
    """ function to render new article page """
    context_dict = {}
    new_article = ArticleForm()
    photoformset = formset_factory(PhotoForm)
    #photoformset = inlineformset_factory()
    formset = photoformset()
    user = request.user
    writer = Writer.objects.filter(user=user).first()
    context_dict['formset'] = formset
    context_dict['writer'] = writer
    context_dict['newarticle'] = new_article
    return render(request, "new-article.html",context_dict)

@login_required
def all_articles_page(request):
    context_dict = {}
    articles = NewsArticle.objects.all().order_by('-time')
    paginator = Paginator(articles, 12) # limitting whats shown
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    context_dict['articles'] = articles
    return render(request, "all-articles.html", context_dict)

@login_required
def new_writer(request):
    """ function to render new writer page """
    context_dict = {}
    writer_form = WriterForm()
    user = request.user
    writer = Writer.objects.filter(user=user).first()
    all_writers = Writer.objects.all()
    writer_stats = []
    for a_writer in all_writers:
        articles = NewsArticle.objects.filter(writer = a_writer)
        writer_stats.append({'writer':a_writer,'articles':len(articles)})
    context_dict['writer'] = writer
    context_dict['writerform'] = writer_form
    context_dict['writers'] = writer_stats
    return render(request, "new-writer.html", context_dict)

@login_required
def new_category(request):
    """ function to render new category page """
    context_dict = {}
    new_category = CategoryForm()
    user = request.user
    writer = Writer.objects.filter(user=user).first()

    context_dict['writer'] = writer
    context_dict['category'] = new_category
    context_dict['categories'] = get_category_data()
    return render(request, "new-category.html", context_dict)

@login_required
def review_page(request):
    """ function to render """
    context_dict = {}
    un_reviewed_articles = NewsArticle.objects.filter(reviewed = False).order_by('-time')
    un_reviewed_comments = Comments.objects.filter(reviewed = False).order_by('-time')
    user = request.user
    writer = Writer.objects.filter(user=user).first()

    context_dict['writer'] = writer
    context_dict['unreviewedArticles'] = un_reviewed_articles
    context_dict['unreviewedComments'] = un_reviewed_comments
    return render(request,"review-page.html", context_dict)

@login_required
def delete_article(request):
    """ function to delete an article """
    articleId = request.POST.get('articleId','')
    article = NewsArticle.objects.filter(id = articleId).first()
    article.delete()
    return HttpResponse("deleted")

def category(request,category_id):
    """ function to sort articles based on categories """
    context_dict = {}
    categories = Category.objects.all()
    category_choosen = Category.objects.filter(id = category_id)
    all_Articles = NewsArticle.objects.filter(reviewed=True,category = category_choosen)
    paginator = Paginator(all_Articles, 12) # limitting whats shown
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    context_dict['articles'] = articles
    context_dict['categories'] = categories
    return render(request,'main-page.html',context_dict)

def single_article(request,article_id=None, link=None):
    """ Function to display requested article """
    context_dict = {}

    date = datetime.today()
    context_dict['weekday'] = date.strftime("%A")
    context_dict['today']  = date.strftime("%d")
    context_dict['month'] = date.strftime("%B")
    context_dict['year'] = date.strftime("%Y")

    print "in the single article function"
    if link:
        article = NewsArticle.objects.filter(link=link).first()
        if article:
            #category = article.category
            relatedArticles = NewsArticle.objects.filter(reviewed=True,category=article.category).exclude(link=link).order_by('-time')[:4]
            comments = Comments.objects.filter(article=article, reviewed=True).order_by('-time')
            if comments:
                is_comments = True
            else:
                is_comments = False

            context_dict['comments'] = comments
            context_dict['iscomments'] = is_comments

            categories = Category.objects.all()
            popular_Articles = NewsArticle.objects.filter(reviewed = True).order_by('-time')[:4]

            context_dict['popularArticles'] = popular_Articles
            context_dict['categories'] = categories
            context_dict['article'] = article
            context_dict['photos'] = NewsPhoto.objects.filter(article = article)
            context_dict['related'] = relatedArticles
            return render(request, "single-article.html",context_dict)
        else:
            return redirect('/')
    else:
        article = NewsArticle.objects.filter(id=article_id).first()
        if article:
            return redirect('/article/'+ article.link)
        else:
            return redirect('/')

@login_required
def add_category(request):
    """ function of adding new category """
    context_dict = {}
    if request.method == 'POST':
        new_category = CategoryForm(request.POST)
        if new_category.is_valid():
            category = new_category.save()
            # clearing categoryform
            new_category = CategoryForm()
    context_dict['category'] = new_category
    context_dict['categories'] = get_category_data()
    return render(request, "new-category.html", context_dict)

@login_required
def delete_category(request,category_id):
    """ Function to delete_category """
    context_dict = {}
    new_category = CategoryForm()
    category = Category.objects.filter(id = category_id).first()
    category.delete()
    context_dict['category'] = new_category
    context_dict['categories'] = get_category_data()
    return render(request, "new-category.html", context_dict)

@login_required
def add_writer(request):
    """ Function to add new writer """
    context_dict = {}
    form = WriterForm()
    if request.method == 'POST':
        form = WriterForm(request.POST, request.FILES)
        if form.is_valid():
            profile = form.cleaned_data['profile']
            first_name = form.cleaned_data['first_name']
            second_name = form.cleaned_data['second_name']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            bio = form.cleaned_data['bio']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            Re_password = form.cleaned_data['Re_password']
            if  password == Re_password:
                pass
            else:
                return HttpResponse("Password didn't match")
            try:
                user = User.objects.create_user(username = username, password = password)
                writer = Writer.objects.create(user = user,
                                               profile = profile,
                                               first_name = first_name,
                                               second_name= second_name,
                                               email = email,
                                               phone = phone,
                                               bio = bio,)
                form = WriterForm()
            except IntegrityError, e:
                return HttpResponse("This User already exists")
            except Exception, e:
                return HttpResponse("Server Error contact system Developers")


    all_writers = Writer.objects.all()
    writer_stats = []
    for a_writer in all_writers:
        articles = NewsArticle.objects.filter(writer = a_writer)
        writer_stats.append({'writer':a_writer,'articles':len(articles)})
    context_dict['writerform'] = form
    context_dict['writers'] = writer_stats
    return render(request,"new-writer.html",context_dict)

@login_required
def add_article(request):
    """ Function to add new article in the DB """
    context_dict = {}
    new_article = ArticleForm()
    photoformset = formset_factory(PhotoForm)

    if request.method == 'POST':
        new_article = ArticleForm(request.POST,request.FILES)
        formset = photoformset(request.POST, request.FILES)
        if new_article.is_valid():
            article = new_article.save(commit= False)
            writer = Writer.objects.filter(user= request.user).first()
            article.writer = writer
            article.time = datetime.now()
            title_link = article.title
            print "Article ID for the new article is "
            print article.id
            article.link = title_link.replace(" ","-")
            article.save()
            """ saving photos from a formset """
            for form in formset:
                if form.is_valid():
                    photos = form.save(commit=False)
                    photos.article = article
                    if photos.image_description:
                        photos.save()
                    else:
                        pass
            message = writer.first_name +" " +writer.second_name + " submmitted new article with title " + article.title + " of article id " + str(article.id)

            try:
                slack_notification("new-article",message)
            except Exception,e:
                pass
            un_reviewed = NewsArticle.objects.filter(reviewed = False)
            context_dict['unreviewed'] = un_reviewed
            return render(request, "review-page.html", context_dict)
    context_dict['formset'] = formset
    context_dict['newarticle'] = new_article
    return render(request, "new-article.html",context_dict)

@login_required
def review_article(request,article_id):
    """ Function to render unreviewed article """
    context_dict = {}
    article = NewsArticle.objects.filter(id=article_id).first()
    photos = NewsPhoto.objects.filter(article = article)
    context_dict['photos'] = photos
    context_dict['article'] = article
    return render(request, "review.html", context_dict)

@login_required
def reviewed(request):
    """ function to review an article """
    article_id = request.POST.get('articleId')
    article = NewsArticle.objects.filter(id = article_id).first()
    article.reviewed = True
    article.save()
    writer = Writer.objects.filter(user = request.user).first()
    message = article.title + " has been reviewed successfully by " + writer.first_name + " " + writer.second_name
    try:
        slack_notification("reviewed-article",message)
    except Exception,e:
        pass
    return HttpResponse("reviewed")

@login_required
def edit_article(request,articleId=1):
    """ function to edit an article """
    context_dict = {}
    #photoformset = formset_factory(PhotoForm)
    #article_photos_instance = NewsArticle.objects.get(pk = articleId)
    #print article_photos_instance
    #PhotoInlineFormSet = inlineformset_factory(NewsArticle,NewsPhoto, fields=('image','image_description',),extra=0)
    if request.method == 'POST':
        article_id = request.POST.get('article_id','')
        instance = get_object_or_404(NewsArticle, id=article_id)
        new_article = ArticleForm(request.POST, request.FILES or None, instance = instance)

        # formset instances
        #formset = PhotoInlineFormSet(request.POST, request.FILES, instance=article_photos_instance)
        # checcking if it is valid
        if new_article.is_valid():
            article = new_article.save(commit=False)
            article.time = datetime.now()
            article.save()
            '''
            print "**** print a formset ****"
            print formset
            if formset.is_valid():
                formset.save()
                print "saved the formset"
            '''
            un_reviewed = NewsArticle.objects.filter(reviewed = False)
            context_dict['unreviewed'] = un_reviewed
            return render(request, "review-page.html", context_dict)

    new_article = ArticleForm()
    instance = get_object_or_404(NewsArticle, id=articleId)
    new_article = ArticleForm(instance = instance)
    #formset = PhotoInlineFormSet(instance=article_photos_instance)
    #context_dict['formset'] = formset
    context_dict['articleId'] = articleId
    context_dict['newarticle'] = new_article
    return render(request,"edit-article.html", context_dict)


@login_required
def edit_articlePhotos(request,photoId=1):
    """function to edit article photos paragraph """
    context_dict = {}
    if request.method == "POST":
        photoId = request.POST.get('photo_Id','')
        print photoId
        instance = get_object_or_404(NewsPhoto,id = photoId)
        photoform = ArticlePhotosForm(request.POST, request.FILES or None, instance= instance)
        if photoform.is_valid():
            photoform.save()
            un_reviewed = NewsArticle.objects.filter(reviewed = False)
            context_dict['unreviewed'] = un_reviewed
            return render(request, "review-page.html", context_dict)

    photoform = ArticlePhotosForm()
    instance = get_object_or_404(NewsPhoto,id=photoId)
    articleId = instance.article.id
    photoform = ArticlePhotosForm(instance = instance)
    context_dict["photoId"] = photoId
    context_dict["articleId"] = articleId
    context_dict['articlePhoto'] = photoform
    return render(request,"edit-article-photos.html",context_dict)

@login_required
def edit_category(request,category_id=1):
    """ function to edit a specific Category """
    context_dict = {}
    if request.method == 'POST':
        category_id = request.POST.get('category_id')
        instance = get_object_or_404(Category,id=category_id)
        edit_category = CategoryForm(request.POST or None, instance = instance)
        #checking if
        if edit_category.is_valid():
            category = edit_category.save()
            new_category = CategoryForm()
            context_dict['category'] = new_category
            context_dict['categories'] = get_category_data()
            return render(request, "new-category.html", context_dict)

    edit_category = CategoryForm()
    instance = get_object_or_404(Category,id=category_id)
    edit_category = CategoryForm(instance = instance)
    context_dict['edit_category'] = edit_category
    context_dict['id'] = category_id
    return render(request,"edit-category.html",context_dict)

def search_articles(request):
    context_dict = {}
    search = request.GET.get('search-article','')
    all_Articles = NewsArticle.objects.filter(title__contains = search,reviewed=True).order_by('-time')
    if search:
        if len(all_Articles) == 0:
            context_dict['message'] = "No results for " + search
        elif len(all_Articles) == 1:
            context_dict['message'] = "result for " + search
        else:
            context_dict['message'] = "results for " + search
    else:
        pass

    paginator = Paginator(all_Articles, 12) # limitting whats shown
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    '''
    if len(articles) == 0:
        context_dict['message'] = "No results for " + search
    else:
        context_dict['message'] = "results for " + search
    '''
    context_dict['categories'] = Category.objects.all()
    context_dict['articles'] = articles
    return render(request, "main-page.html", context_dict)

def feedback_function(request):
    """
    Function to receive feedback from users and send a notification to slack
    """
    message = request.POST.get('message')
    email = request.POST.get('email')
    names = request.POST.get('names')
    message_to_send = names + " sent this message : " + message + ", the sender's email is " + email
    slack_notification("feedback-messages",message_to_send)
    return HttpResponse("Thank you for Sending a message to Us!")


def comment_func(request):
    """
    Function to save comments in the DB
    """
    commentedArticleId = request.POST.get('commentedArticle')
    commentName = request.POST.get('commentName')
    commentEmail = request.POST.get('commentEmail')
    commentMessage = request.POST.get('commentMessage')

    commentedArticle = NewsArticle.objects.filter(id=commentedArticleId).first()
    try:
        comment = Comments.objects.create(article = commentedArticle,
                                          name = commentName,
                                          email = commentEmail,
                                          comment = commentMessage,
                                          time = datetime.now(),
                                          reviewed = False,)
        return HttpResponse("Thanks "+ comment.name + " for your comment, It will appear in comments after being reviewed")
    except Exception, e:
        return HttpResponse("Ooops, Something went wrong, Try again later")

    return HttpResponse("on comment Function")

@login_required
def review_comments(request,commentId):
    """
    Function to review submmitted comments
    """
    context_dict = {}
    comment = Comments.objects.filter(id=commentId).first()
    comment.reviewed = True
    comment.save()

    un_reviewed_articles = NewsArticle.objects.filter(reviewed = False).order_by('-time')
    un_reviewed_comments = Comments.objects.filter(reviewed = False).order_by('-time')
    user = request.user
    writer = Writer.objects.filter(user=user).first()

    context_dict['writer'] = writer
    context_dict['unreviewedArticles'] = un_reviewed_articles
    context_dict['unreviewedComments'] = un_reviewed_comments
    return render(request,"review-page.html", context_dict)
