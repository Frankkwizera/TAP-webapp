from django.contrib import admin

# Register your models here.

from campusweb.models import *

admin.site.register(Writer)
admin.site.register(Category)
admin.site.register(NewsArticle)
admin.site.register(NewsPhoto)
admin.site.register(Comments)
