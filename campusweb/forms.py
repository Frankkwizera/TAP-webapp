from django.forms import ModelForm
from campusweb.models import *
from django import forms

class WriterForm(forms.Form):
    profile = forms.FileField(label="profile pic")
    first_name = forms.CharField(label = "first name",max_length=40, required=True)
    second_name = forms.CharField(label="second name",max_length=60, required=True)
    email = forms.CharField(label="email",max_length=40)
    phone = forms.IntegerField(label="phone number")
    bio = forms.CharField(label="biography", max_length=200)
    username = forms.CharField(label="username",max_length=40)
    password = forms.CharField(label="password",max_length=40)
    Re_password = forms.CharField(label="re-type password",max_length=40)

    class Meta:
           widgets = {
        'password': forms.PasswordInput(),
        'Re_password': forms.PasswordInput(),
    }

class ArticleForm(ModelForm):

    error_css_class = "warning"
    required_css_class = "info"

    class Meta:
        model = NewsArticle
        exclude = ('writer','reviewed','time','link',)

class ArticlePhotosForm(ModelForm):

    error_css_class = "warning"
    required_css_class = "info"

    class Meta:
        model = NewsPhoto
        exclude = ('article',)

class CategoryForm(ModelForm):

    error_css_class = "warning"
    required_css_class = "info"

    class Meta(object):
        model = Category
        fields = ('__all__')

class PhotoForm(ModelForm):
    error_css_class = "warning"
    required_css_class = "info"

    class Meta(object):
        model = NewsPhoto
        exclude = ('article','')
