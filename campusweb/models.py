from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Writer(models.Model):
    """info About Writers """
    user = models.OneToOneField(User)
    profile = models.FileField(upload_to='media',help_text="profile picture",null=True,blank=True)
    first_name = models.CharField(max_length=40)
    second_name = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    phone = models.IntegerField()
    bio = models.CharField(max_length=200,null=True,blank=True,help_text="short biography")
    is_admin = models.BooleanField(default=False)

    def __str__(self):
        return str(self.first_name) + " " + str(self.second_name)

class Category(models.Model):
    """ Info about categories in the Mag """
    category_name = models.CharField(max_length=20,unique= True)
    category_description = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name


class NewsArticle(models.Model):
    """ News Table """
    writer = models.ForeignKey(Writer)
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=10000)
    conclusion = models.TextField(max_length=1000, null = True,blank = True)
    category = models.ForeignKey(Category)
    title_image = models.FileField(upload_to='media', help_text="Insert a photo to be displayed as a heading image")
    reviewed = models.BooleanField(default=False)
    time = models.DateTimeField(default=0)
    link = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.title

class NewsPhoto(models.Model):
    """ Table containing photos for the new article """
    article = models.ForeignKey(NewsArticle)
    image = models.FileField(upload_to='media', help_text="upload a photo related to this article")
    image_description = models.TextField(max_length=1000)

    def __str__(self):
        return self.image_description

class Comments(models.Model):
    """
    Table containing comments
    sent by visitors
    """
    article = models.ForeignKey(NewsArticle)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=40)
    comment = models.TextField(max_length=1000)
    time = models.DateTimeField(null = True)
    reviewed = models.BooleanField(default=False)

    def __str__(self):
        return self.comment
