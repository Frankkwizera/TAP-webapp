from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from campusweb.models import *
from campusweb.forms import *
from slacker import Slacker

def main_page_data(request):
    all_Articles = NewsArticle.objects.filter(reviewed=True).order_by('-time')
    paginator = Paginator(all_Articles, 20) # limitting whats shown
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    return articles

def get_admin_data():
    context_dict = {}
    writer_form = WriterForm()
    new_article = ArticleForm()
    new_category = CategoryForm()

    un_reviewed = NewsArticle.objects.filter(reviewed = False)
    context_dict['unreviewed'] = un_reviewed
    #context_dict['newarticle'] = new_article
    context_dict['writerform'] = writer_form
    context_dict['category'] = new_category
    context_dict['categories'] = Category.objects.all()
    return context_dict

def get_category_data():
    result = []
    categories = Category.objects.all()
    for category in categories:
        articles = NewsArticle.objects.filter(category = category)
        result.append({'category':category,'length':len(articles)})
    return result

"""
send notification via slack
"""
def slack_notification(channel,message):
    slack = Slacker('xoxp-69628191409-69623864309-69867602944-b02141c9ab')

    if channel.startswith('#'):
        pass
    else:
        channel = '#'+channel

    slack.chat.post_message(channel,message)

"""
function to send messages via email
from user's feedback
"""
#def send_email(text):

"""
  <div class="navigation-section">
<nav class="navbar m-menu navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1"><span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="#navbar-collapse-1">
            <ul class="nav navbar-nav main-nav">
                <li class="active"><a href="/">News</a></li>
                <li class="dropdown m-menu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Category
                    <span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="m-menu-content">
                                <ul class="col-sm-12">
                                  {% for category in categories %}
                                  <li><a href="/category/{{category.id}}">{{category.category_name}}</a></li>
                                  {% endfor %}
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </div>
        <!-- .navbar-collapse -->
    </div>
    <!-- .container -->
</nav>
<!-- .nav -->
</div>
"""
